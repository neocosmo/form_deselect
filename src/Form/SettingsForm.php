<?php

namespace Drupal\form_deselect\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration settings form for form_deselect.
 */
class SettingsForm extends ConfigFormBase {

  const SETTINGS = 'form_deselect.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'form_deselect_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Form constructor.
    $form = parent::buildForm($form, $form_state);

    $form['deselect_forms'] = [
      '#type' => 'container',
      '#tree' => TRUE,
      '#description' => $this->t("Define all form's that should use deselect elements. You need to provide the form's ID and you can specify form elements, that should not show up in the deselect element."),
    ];

    $values = $form_state->getValues();
    $config = $this->configToValues($values['deselect_forms'] ?? NULL);

    $triggering_element = $form_state->getTriggeringElement();
    $trigger_name = $triggering_element['#name'] ?? '';
    if (!$triggering_element) {
      $config[] = [];
      foreach (array_keys($config) as $i) {
        $config[$i]['ignore_fields'][] = [''];
      }
    }
    elseif (substr($trigger_name, -8) === 'add_more') {
      if (substr($trigger_name, 0, 15) === 'deselect_forms:') {
        $config[] = [
          'form_id' => '',
          'ignore_fields' => [''],
        ];
      }
      elseif (substr($trigger_name, 0, 14) === 'ignore_fields:') {
        list(, $index) = explode(':', $trigger_name);
        $config[$index]['ignore_fields'][] = '';
      }
    }

    foreach ($config as $form_index => $form_settings) {
      $form_id = $form_settings['form_id'] ?? '';
      $ignore_fields = $form_settings['ignore_fields'] ?? [];
      $form['deselect_forms'][$form_index]
        = $this->formElement($form_index, $form_id, $ignore_fields);
    }

    $form['deselect_forms']['deselect_forms:add_more'] = [
      '#type' => 'button',
      '#value' => $this->t('Add more'),
      '#name' => 'deselect_forms:add_more',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (substr($form_state->getTriggeringElement()['#name'], -8) === 'add_more') {
      $form_state->setRebuild();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);
    $deselect_forms = [];
    $values = $form_state->getValue('deselect_forms') ?? [];
    foreach ($values as $index => $form_settings) {
      if (!is_numeric($index) || !is_array($form_settings)
        || empty($form_settings['form_id'])) {

        continue;
      }
      $ignore_fields = $form_settings['ignore_fields'] ?? [];
      $ignore_fields = array_filter($ignore_fields, 'is_numeric',
        ARRAY_FILTER_USE_KEY);
      $ignore_fields = array_filter($ignore_fields);
      $deselect_forms[$form_settings['form_id']] = [
        'ignore_fields' => $ignore_fields,
      ];
    }
    $config->set('deselect_forms', $deselect_forms);
    $config->save();
    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * Get a form element for a form's settings.
   *
   * @param int $index
   *   The index of the form element.
   * @param string $form_id
   *   Optional. The ID of the form. Defaults to an empty string.
   * @param array $ignore_fields
   *   Optional. An array of input element names, that should be ignored by the
   *   deselect element.
   *
   * @return array
   *   A render array for configuring a deselect form.
   */
  protected function formElement(
    int $index,
    string $form_id = '',
    array $ignore_fields = []) {

    $element = [
      '#type' => 'fieldset',
      '#title' => $this->t('Form'),
      'form_id' => [
        '#type' => 'textfield',
        '#title' => $this->t('Form ID'),
        '#default_value' => $form_id,
      ],
      'ignore_fields' => [
        '#type' => 'fieldset',
        '#title' => $this->t('Ignore field list'),
      ],
    ];
    foreach ($ignore_fields as $ignore_field_id) {
      $element['ignore_fields'][] = [
        '#type' => 'textfield',
        '#title' => $this->t('Field name'),
        '#default_value' => $ignore_field_id,
      ];
    }
    $element['ignore_fields']['ignore_fields:' . $form_id . ':add_more'] = [
      '#type' => 'button',
      '#value' => $this->t('Add more'),
      '#name' => 'ignore_fields:' . $index . ':add_more',
    ];
    return $element;
  }

  /**
   * Get the deselect form configuration.
   *
   * @param array $form_values
   *   Optional. Values from the form state for the deselect forms. If these are
   *   NULL, the values are loaded from the module's configuration.
   *
   * @return array
   *   An array containing the deselect form configuration values. The array
   *   contains an array for each form configuration. Each form configuration
   *   array has the keys 'form_id' and 'ignore_fields'.
   */
  protected function configToValues(array $form_values = NULL) {
    if ($form_values !== NULL) {
      $values = array_filter($form_values, 'is_numeric', ARRAY_FILTER_USE_KEY);
      foreach ($values as &$value) {
        $value['ignore_fields'] = array_filter($value['ignore_fields'],
          'is_numeric', ARRAY_FILTER_USE_KEY);
      }
      return $values;
    }

    $config = $this->config(static::SETTINGS);
    $values = [];
    $deselect_forms = $config->get('deselect_forms') ?? [];
    foreach ($deselect_forms as $form_id => $form_settings) {
      $values[] = [
        'form_id' => $form_id,
      ] + $form_settings;
    }
    return $values;
  }

}
